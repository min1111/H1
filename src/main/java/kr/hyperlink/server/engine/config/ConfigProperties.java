/*
 * engine
 * Copyright 2019. 4. 11. Hyper Link all rights reserved.
 */

 

package kr.hyperlink.server.engine.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * <pre>
 * kr.hyperlink.server.engine.config 
 * ConfigProperties.java
 * @author 			:	Hyper Link 심현섭
 * @date 			:	2019. 4. 11.
 * @brief			:   서버 Properties (파일설정) 처리 클래스
 * @description		:	
 * </pre>
 *
 */
@Component
public class ConfigProperties {
	
	@Value("${server.admin.front_url}")
	private String frontDomain;
	
	@Value("${server.admin.file_path}")
	private String filePath;
	
	/**
	 * @brief frontDomain를 전달한다.
	 *
	 * @return frontDomain
	 */
	public String getFrontDomain() {
		return frontDomain;
	}
	
	/**
	 * @brief filePath를 전달한다.
	 *
	 * @return filePath
	 */
	public String getFilePath() {
		return filePath;
	}

}
