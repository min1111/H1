/*
 * engine
 * Copyright 2019. 4. 10. Hyper Link all rights reserved.
 */

 

package kr.hyperlink.server.engine.config;

import org.apache.catalina.connector.Connector;
import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.multipart.MultipartResolver;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;

/**
 * <pre>
 * kr.hyperlink.server.engine.config 
 * WebServletConfigration.java
 * @author 			:	Hyper Link 심현섭
 * @date 			:	2019. 4. 10.
 * @brief			:   Spring boot 웹 설정
 * @description		:	
 * </pre>
 *
 */
@Configuration
public class WebServletConfigration {
	
	@Bean
	public MultipartResolver multipartResolver() {
	    org.springframework.web.multipart.commons.CommonsMultipartResolver multipartResolver = new org.springframework.web.multipart.commons.CommonsMultipartResolver();
	    multipartResolver.setMaxUploadSize(999000000);
	    multipartResolver.setDefaultEncoding("UTF-8");
	    return multipartResolver;
	}

}
