/*
 * engine
 * Copyright 2019. 4. 10. Hyper Link all rights reserved.
 */

 

package kr.hyperlink.server.engine.security;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

/**
 * <pre>
 * kr.hyperlink.server.engine.security 
 * RestAuthenticationEntryPoint.java
 * @author 			:	Hyper Link 심현섭
 * @date 			:	2019. 4. 10.
 * @brief			:   비 인가 접근 처리 클래스
 * @description		:	
 * </pre>
 *
 */
@Component
public class RestAuthenticationEntryPoint implements AuthenticationEntryPoint {
	
	private static final Logger log = LoggerFactory.getLogger(RestAuthenticationEntryPoint.class);

	@Override
	public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException) throws IOException, ServletException {
		log.debug("Unauthorized");
		response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Unauthorized");
	}

}
