/*
 * engine
 * Copyright 2019. 4. 10. Hyper Link all rights reserved.
 */

 

package kr.hyperlink.server.engine.security;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

/**
 * <pre>
 * kr.hyperlink.server.engine.security 
 * LoginFaiureHandler.java
 * @author 			:	Hyper Link 심현섭
 * @date 			:	2019. 4. 10.
 * @brief			:   로그인 실패시 처리 클래스
 * @description		:	
 * </pre>
 *
 */
@Slf4j
@Component
public class LoginFaiureHandler implements AuthenticationFailureHandler {
	
	@Autowired
	private AuthenticationService service;

	@Override
	public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception) throws IOException, ServletException {
		if (log.isDebugEnabled()) log.debug("begin LoginFaiureHandler#onAuthenticationFailure");
		if (log.isDebugEnabled()) log.debug("begin LoginFaiureHandler#determineTargetUrl");
		service.failedLogin(request, response);
	}

}
