/*
 * engine
 * Copyright 2019. 4. 11. Hyper Link all rights reserved.
 */

 

package kr.hyperlink.server.engine.security;

import java.util.Map;

/**
 * <pre>
 * kr.hyperlink.server.engine.security 
 * AuthenticationModel.java
 * @author 			:	Hyper Link 심현섭
 * @date 			:	2019. 4. 11.
 * @brief			:   인증처리 모델
 * @description		:	
 * </pre>
 *
 */

public class AuthenticationModel {

	
	/** <code>message</code> : 에러 메세지 */
	private String message;
	
	/** <code>error</code> : 에러여부 */
	private boolean error;
	
	/** <code>statusCode</code> : 에러 코드  */
	private int statusCode;
	
	/** <code>authToken</code> : 인증 토큰 */
	private String authToken;
	
	/** <code>data</code> : 에러 메세지 맵핑 데이터  */
	private Map<String, Object> data;
	
	public AuthenticationModel() {}
	
	public AuthenticationModel(String message, int statusCode) {
		this(message, false, statusCode);
	}

	public AuthenticationModel(String message, boolean error, int statusCode) {
		this.message = message;
		this.error = error;
		this.statusCode = statusCode;
	}

	/**
	 * @brief message를 전달한다.
	 *
	 * @return message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @brief message을 설정한다. 
	 *
	 * @param message message 
	 */
	
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * @brief error를 전달한다.
	 *
	 * @return error
	 */
	public boolean isError() {
		return error;
	}

	/**
	 * @brief error을 설정한다. 
	 *
	 * @param error error 
	 */
	
	public void setError(boolean error) {
		this.error = error;
	}

	/**
	 * @brief statusCode를 전달한다.
	 *
	 * @return statusCode
	 */
	public int getStatusCode() {
		return statusCode;
	}

	/**
	 * @brief statusCode을 설정한다. 
	 *
	 * @param statusCode statusCode 
	 */
	
	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}

	/**
	 * @brief data를 전달한다.
	 *
	 * @return data
	 */
	public Map<String, Object> getData() {
		return data;
	}

	/**
	 * @brief data을 설정한다. 
	 *
	 * @param data data 
	 */
	
	public void setData(Map<String, Object> data) {
		this.data = data;
	}

	/**
	 * @brief authToken를 전달한다.
	 *
	 * @return authToken
	 */
	public String getAuthToken() {
		return authToken;
	}

	/**
	 * @brief authToken을 설정한다. 
	 *
	 * @param authToken authToken 
	 */
	
	public void setAuthToken(String authToken) {
		this.authToken = authToken;
	}
}
