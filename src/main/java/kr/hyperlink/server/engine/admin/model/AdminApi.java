/*
 * engine
 * Copyright 2019. 4. 11. Hyper Link all rights reserved.
 */

 

package kr.hyperlink.server.engine.admin.model;

import org.springframework.data.domain.Page;

import kr.hyperlink.server.engine.api.model.UserApi;

/**
 * <pre>
 * kr.hyperlink.server.engine.admin.model 
 * AdminApi.java
 * @author 			:	Hyper Link 심현섭
 * @date 			:	2019. 4. 11.
 * @brief			:   관리자 반환 모델
 * @description		:	
 * </pre>
 *
 */

public class AdminApi<T> extends UserApi<T> {

	/** <code>serialVersionUID</code> : */
	private static final long serialVersionUID = -7423475268127162181L;
	
	public AdminApi() {
		super();
	}
	
	public AdminApi(String resStatus) {
		super(resStatus);
	}
	
	public AdminApi(Page<T> value) {
		super(value);
	}

}
