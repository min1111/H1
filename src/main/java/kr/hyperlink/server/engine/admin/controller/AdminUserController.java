/**
 * 
 */
package kr.hyperlink.server.engine.admin.controller;

import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import kr.hyperlink.server.engine.admin.domain.AdminUser;
import kr.hyperlink.server.engine.admin.model.AdminApi;
import kr.hyperlink.server.engine.admin.service.AdminUserService;
import kr.hyperlink.server.engine.exception.AdminControllerException;
import lombok.extern.slf4j.Slf4j;

/**
 * <pre>
 * kr.hyperlink.server.engine.admin.controller 
 * AdminUserController.java
 * @author 			:	Hyper Link 심현섭
 * @date 				:	2020. 6. 24.
 * @brief				:   관리자 관리 Controller
 * @description		:	
 * </pre>
 *
 */
@Slf4j
@RestController
@RequestMapping(value = "/admin/userManagement")
public class AdminUserController {

	private AdminUserService service;
	
	public AdminUserController (AdminUserService service) {
		this.service = service;
	}
	
	@GetMapping
	public AdminApi<AdminUser> getSuperManagement (
			@RequestParam(value = "page", required = false, defaultValue = "1") int page,
			@RequestParam(value = "max", required = false, defaultValue = "15") int max,
			@RequestParam(value = "searchRoleType", required = false, defaultValue = "ROLE_SUPER_MANAGER") String roleType,
			@RequestParam(value = "searchStatus", defaultValue = "ALL", required=false) String status,
			@RequestParam(value = "searchText", defaultValue = "", required=false) String search,
			@RequestParam(value = "searchType", defaultValue = "ALL", required=false) String type) throws AdminControllerException {
		AdminApi<AdminUser> rtn = null;
		try {
			rtn = service.getUserManagement(page, max, status, type, search, roleType);
		} catch (AdminControllerException adminEx) {
			log.error("[ An error occueerd due to a code {} ]", adminEx.getCode());
			throw new AdminControllerException(adminEx.getCode());
		} catch (Exception e) {
			log.error("[ An error occueerd due to a {} ]", e);
			throw new AdminControllerException(AdminApi.RES_STATUS_CODE_FOR_ERROR_IS_SERVER_ERROR, e.toString());
		}
		return rtn;
	}
	
	@PostMapping
	public AdminApi<?> createAdmin (@RequestBody AdminUser adminUser) throws AdminControllerException {
		AdminApi<?> rtn = null;
		try {
			rtn = service.createAdmin(adminUser);
		} catch (AdminControllerException adminEx) {
			log.error("[ An error occueerd due to a code {} ]", adminEx.getCode());
			throw new AdminControllerException(adminEx.getCode());
		} catch (Exception e) {
			log.error("[ An error occueerd due to a {} ]", e);
			throw new AdminControllerException(AdminApi.RES_STATUS_CODE_FOR_ERROR_IS_SERVER_ERROR, e.toString());
		}
		return rtn;
	}
	
	@GetMapping(value = "/{id}")
	public AdminApi<AdminUser> getAdmin (@Validated @PathVariable("id") long id) throws AdminControllerException {
		AdminApi<AdminUser> rtn = null;
		try {
			rtn = service.getAdminUser(id);
		} catch (AdminControllerException adminEx) {
			log.error("[ An error occueerd due to a code {} ]", adminEx.getCode());
			throw new AdminControllerException(adminEx.getCode());
		} catch (Exception e) {
			log.error("[ An error occueerd due to a {} ]", e);
			throw new AdminControllerException(AdminApi.RES_STATUS_CODE_FOR_ERROR_IS_SERVER_ERROR, e.toString());
		}
		return rtn;
	}
	
	@PutMapping(value = "/{id}")
	public AdminApi<?> updateAdmin (@Validated @PathVariable("id") long id, @RequestBody AdminUser adminUser) throws AdminControllerException {
		AdminApi<?> rtn = null;
		try {
			rtn = service.updateAdmin(id, adminUser);
		} catch (AdminControllerException adminEx) {
			log.error("[ An error occueerd due to a code {} ]", adminEx.getCode());
			throw new AdminControllerException(adminEx.getCode());
		} catch (Exception e) {
			log.error("[ An error occueerd due to a {} ]", e);
			throw new AdminControllerException(AdminApi.RES_STATUS_CODE_FOR_ERROR_IS_SERVER_ERROR, e.toString());
		}
		return rtn;
	}
	
	@DeleteMapping(value = "/{id}")
	public AdminApi<?> deleteAdmin (@Validated @PathVariable("id") long id) throws AdminControllerException {
		AdminApi<?> rtn = null;
		try {
			rtn = service.deleteAdmin(id);
		} catch (AdminControllerException adminEx) {
			log.error("[ An error occueerd due to a code {} ]", adminEx.getCode());
			throw new AdminControllerException(adminEx.getCode());
		} catch (Exception e) {
			log.error("[ An error occueerd due to a {} ]", e);
			throw new AdminControllerException(AdminApi.RES_STATUS_CODE_FOR_ERROR_IS_SERVER_ERROR, e.toString());
		}
		return rtn;
	}
}
