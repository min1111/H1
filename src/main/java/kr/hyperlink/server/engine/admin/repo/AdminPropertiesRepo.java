/*
 * engine
 * Copyright 2019. 4. 11. Hyper Link all rights reserved.
 */

 

package kr.hyperlink.server.engine.admin.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import kr.hyperlink.server.engine.admin.domain.AdminProperties;

/**
 * <pre>
 * kr.hyperlink.server.engine.admin.repo 
 * AdminPropertiesRepo.java
 * @author 			:	Hyper Link 심현섭
 * @date 			:	2019. 4. 11.
 * @brief			:   사이트 관리 Repo
 * @description		:	
 * </pre>
 *
 */

public interface AdminPropertiesRepo  extends JpaRepository<AdminProperties, Long> { 

}
