/*
 * engine
 * Copyright 2019. 4. 11. Hyper Link all rights reserved.
 */

 

package kr.hyperlink.server.engine.api.service;

import java.net.HttpURLConnection;
import java.util.Arrays;
import java.util.Date;
import java.util.UUID;

import javax.transaction.Transactional;

import kr.hyperlink.server.engine.utility.DateUtility;
import kr.hyperlink.server.engine.utility.FileHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import kr.hyperlink.server.engine.api.domain.ApiUser;
import kr.hyperlink.server.engine.api.domain.ApiUser.LatestLoginType;
import kr.hyperlink.server.engine.api.domain.ApiUserDevice;
import kr.hyperlink.server.engine.api.model.AppModel;
import kr.hyperlink.server.engine.api.model.User;
import kr.hyperlink.server.engine.api.model.UserApi;
import kr.hyperlink.server.engine.api.model.UserResponse;
import kr.hyperlink.server.engine.api.repo.ApiUserDeviceRepo;
import kr.hyperlink.server.engine.api.repo.ApiUserRepo;
import kr.hyperlink.server.engine.exception.ApiControllerException;
import kr.hyperlink.server.engine.utility.DBUtility;
import kr.hyperlink.server.engine.utility.HttpUtility;
import lombok.extern.slf4j.Slf4j;

/**
 * <pre>
 * kr.hyperlink.server.engine.api.service 
 * ApiUserService.java
 * @author 			:	Hyper Link 심현섭
 * @date 			:	2019. 4. 11.
 * @brief			:   
 * @description		:	
 * </pre>
 *
 */
@Slf4j
@Service
public class ApiUserService implements UserDetailsService  {
	
	@Autowired
	private ApiUserDeviceRepo userDeviceRepo;
	
	@Autowired
	private ApiUserRepo userRepo;
	
	@Autowired
	private FileHandler fileHandler;
	
	@Autowired
	private PasswordEncoder encoder; 
	
	
	@Override
	public UserDetails loadUserByUsername(String authToken) throws UsernameNotFoundException {
		ApiUser user = findByAuthToken(authToken);
		if(user == null) {
			throw new UsernameNotFoundException(authToken + " is not found");	
		}
		return user;
	}

	public ApiUser findByAuthToken(String deviceToken) {
		ApiUserDevice device = userDeviceRepo.findByAuthToken(deviceToken);
		if(device != null) {
			return device.getUser();
		}
		return null;
	}
	
	@Transactional(rollbackOn = {Exception.class, ApiControllerException.class})
	public String validateUserToken(String xAuthToken)  throws ApiControllerException {
		return this.validateToken(xAuthToken, null);
	}

	@Transactional(rollbackOn = {Exception.class, ApiControllerException.class})
	public String validateUserToken(String xAuthToken, AppModel appModel) throws ApiControllerException {
		return this.validateToken(xAuthToken, appModel);
		
	}
	
	private String validateToken (String xAuthToken, AppModel model) throws ApiControllerException {
		ApiUserDevice device = userDeviceRepo.findByAuthToken(xAuthToken);
		if(device == null) {
			return UserApi.RES_STATUS_CODE_FOR_FAILED_IS_EXPIRED_USER_TOKEN;
		}
		ApiUser user = device.getUser();
		if(user.getStatus() == ApiUser.UserStatus.FORCED_CHECKOUT) {
			return UserApi.RES_STATUS_CODE_FOR_FAILED_IS_USER_NOT_USE;
		}
		
		
		long loginExpiredDate = device.getLoginExpired().getTime();
		long nowDate = System.currentTimeMillis();
		
		if(loginExpiredDate < nowDate) {
			user.setLogoutTime(new Date());
			user.setStatus(ApiUser.UserStatus.LOGOUT);
			userRepo.save(user);
			return UserApi.RES_STATUS_CODE_FOR_FAILED_IS_EXPIRED_USER_TOKEN;
		}
		if(model != null) {
			device.setAppVersion(model.getVersionInfo());
			userDeviceRepo.save(device);
		}
		return UserApi.RES_STATUS_CODE_FOR_SUCCESSFUL;
		
	}

	@Transactional(rollbackOn = {Exception.class, ApiControllerException.class})
	public UserApi<UserResponse> loginUserSNS(String userId, String password, 
												String loginType, String email, String authToken, 
												String pushId, String deviceId, String modelName, 
												String userName, String osType, String versionInfo, MultipartFile profileImage) throws ApiControllerException, Exception { 
		
		// 파라메터 검증
		ApiUser.LatestLoginType loginTypeEnum = (LatestLoginType) DBUtility.convertToEnumByString(Arrays.asList(ApiUser.LatestLoginType.values()), loginType);
		ApiUserDevice.OsType osTypeEnum = (ApiUserDevice.OsType) DBUtility.convertToEnumByString(Arrays.asList(ApiUserDevice.OsType.values()), osType);
		
		
		if(loginTypeEnum == null || osTypeEnum == null) {
			throw new ApiControllerException(UserApi.RES_STATUS_CODE_FOR_ERROR_IS_INPUT_PARAMS_INVALID);
		}
		
		
		ApiUser loginedUser = null;
		
		ApiUser.LoginType registerType = null;
		
		User reqUser = new User();
		
		reqUser.setUserId(userId);
		reqUser.setPassword(password);
		reqUser.setLoginType(loginTypeEnum);
		reqUser.setEmail(email);
		reqUser.setAuthToken(authToken);
		reqUser.setPushId(pushId);
		reqUser.setDeviceId(deviceId);
		reqUser.setModelName(modelName);
		reqUser.setUserName(userName);
		reqUser.setProfileImage(profileImage);
		reqUser.setOsType(osTypeEnum);
		reqUser.setVersionInfo(versionInfo);
		
		switch(loginTypeEnum) {
		case ETC:{ 
			
			if(password == null) {
				throw new ApiControllerException(UserApi.RES_STATUS_CODE_FOR_ERROR_IS_INPUT_PARAMS_INVALID);
			}
			
			if(password.isEmpty()) {
				throw new ApiControllerException(UserApi.RES_STATUS_CODE_FOR_ERROR_IS_INPUT_PARAMS_INVALID);
			}
			int count = userRepo.countByUsername(reqUser.getUserId());
			if(count > 0) {
				throw new ApiControllerException(UserApi.RES_STATUS_CODE_FOR_FAILED_IS_USER_DUPLICATION_CHECK);
			}
			
			if(email == null) {
				throw new ApiControllerException(UserApi.RES_STATUS_CODE_FOR_ERROR_IS_INPUT_PARAMS_INVALID);
			}
			
			if(email.isEmpty()) {
				throw new ApiControllerException(UserApi.RES_STATUS_CODE_FOR_ERROR_IS_INPUT_PARAMS_INVALID);
			}
			
			
			
			int countByUserEmail = userRepo.countByEmail(reqUser.getEmail());
			
			if(countByUserEmail > 0) {
				throw new ApiControllerException(UserApi.RES_STATUS_CODE_FOR_FAILED_IS_USER_EMAIL_DUPLICATION_CHECK);
			}
			
			registerType = ApiUser.LoginType.REGISTER;
			
			loginedUser = new ApiUser();
		}break;
		
		default:{
			if(authToken == null) {
				throw new ApiControllerException(UserApi.RES_STATUS_CODE_FOR_ERROR_IS_INPUT_PARAMS_INVALID);
			}
			
			if(authToken.isEmpty()) {
				throw new ApiControllerException(UserApi.RES_STATUS_CODE_FOR_ERROR_IS_INPUT_PARAMS_INVALID);
			}
			
			UserApi<?> httpSNS = null;
			HttpUtility http = null;
			
			if(reqUser.getLoginType() == ApiUser.LatestLoginType.FACEBOOK) {
				http = new HttpUtility(HttpUtility.FACEBOOK_API_ME_URL);
				httpSNS = http.sendToFacebookWithGet(reqUser.getAuthToken());
				loginedUser = userRepo.findByUserIDSNS(reqUser.getUserId());
			} else if (reqUser.getLoginType() == ApiUser.LatestLoginType.KAKAO) {
				http = new HttpUtility(HttpUtility.KAKAO_API_URL);
				httpSNS = http.sendToFacebookWithGet(reqUser.getAuthToken());
				loginedUser = userRepo.findByUserIDSNS(reqUser.getUserId());
			} else if (reqUser.getLoginType() == ApiUser.LatestLoginType.NAVER) {
				http = new HttpUtility(HttpUtility.NAVER_API_URL);
				httpSNS = http.sendToFacebookWithGet(reqUser.getAuthToken());
				loginedUser = userRepo.findByUserIDSNS(reqUser.getUserId());
			}
			
			if(httpSNS.getStatus() != HttpURLConnection.HTTP_OK) {
				throw new ApiControllerException(UserApi.RES_STATUS_CODE_FOR_FAILED_IS_FACEBOOK_LOGIN);
			}

			if(loginedUser == null) {
				registerType = ApiUser.LoginType.REGISTER;
				loginedUser = new ApiUser();
			} else {
				registerType = ApiUser.LoginType.LOGIN;
			}
			
		}break;
		}
		
		UserApi<UserResponse> api = new UserApi<UserResponse>();
		
		loginedUser.setRoleType(ApiUser.RoleType.ROLE_USER);
		loginedUser.setLatestLogin(new Date());
		
		if(registerType == ApiUser.LoginType.LOGIN) {
			if(loginedUser.getStatus() == ApiUser.UserStatus.FORCED_CHECKOUT) {
				throw new ApiControllerException(UserApi.RES_STATUS_CODE_FOR_FAILED_IS_USER_NOT_USE);
			}
			loginedUser.setStatus(ApiUser.UserStatus.USE);
		} else {
			
			loginedUser.setStatus(ApiUser.UserStatus.USE);
			
			if(reqUser.getLoginType() == ApiUser.LatestLoginType.FACEBOOK || reqUser.getLoginType() == ApiUser.LatestLoginType.KAKAO || reqUser.getLoginType() == ApiUser.LatestLoginType.NAVER) {
				loginedUser.setUserIDSNS(reqUser.getUserId());
				loginedUser.setEmailSNS(reqUser.getEmail());
			} else {
				loginedUser.setEmail(reqUser.getEmail());
				loginedUser.setUsername(reqUser.getUserId());
				loginedUser.setPassword(encoder.encode(reqUser.getPassword()));
			}
			
			loginedUser.setName(reqUser.getUserName());
			loginedUser.setLoginType(reqUser.getLoginType());
			if(reqUser.getProfileImage() != null) {
				FileHandler fileHandler = new FileHandler();
				String profilePath = fileHandler.createPostImage(reqUser.getProfileImage(), FileHandler.UploadType.PROFILE);
				loginedUser.setProfileImagePath(profilePath);
			}
		}
		
		loginedUser = userRepo.save(loginedUser);
		if(loginedUser == null || loginedUser.getId() == 0) {
			throw new ApiControllerException(UserApi.RES_STATUS_CODE_FOR_ERROR_IS_DB_FAILED);
		}
		
		ApiUserDevice userDevice = registerDevice(loginedUser, reqUser);
		
		UserResponse response = new UserResponse();
		
		response.setUser(loginedUser);
		response.setExpiredDate(userDevice.getLoginExpired());
		response.setAuthToken(userDevice.getAuthToken());
		api.addModel(response);

		return api;
	}
	
	private ApiUserDevice registerDevice(ApiUser loginedUser, User user) throws ApiControllerException, Exception {
		ApiUserDevice userDevice = userDeviceRepo.findByDeviceId(user.getDeviceId());
		if(userDevice == null) {
			userDevice = new ApiUserDevice();
			userDevice.setDeviceId(user.getDeviceId());
		}
		int sessionExpiredTimeForMonth = 2;
		Date loginExpired = new DateUtility().addMonth(sessionExpiredTimeForMonth).getDate();
		String authToken = UUID.randomUUID().toString();
		
		userDevice.setUser(loginedUser);
		userDevice.setAuthToken(authToken);
		userDevice.setAuthTokenForSNS(user.getAuthToken());
		userDevice.setLoginExpired(loginExpired);
		userDevice.setName(user.getModelName());
		userDevice.setAppVersion(user.getVersionInfo());
		
		
		if(user.getOsType() == ApiUserDevice.OsType.IOS) {
			userDevice.setOsType(ApiUserDevice.OsType.IOS);
		} else {
			userDevice.setOsType(ApiUserDevice.OsType.ANDROID);
		}
		
		userDevice = userDeviceRepo.save(userDevice);
		
		if(userDevice == null || userDevice.getId() == 0) {
			throw new ApiControllerException(UserApi.RES_STATUS_CODE_FOR_ERROR_IS_DB_FAILED);
		}
		
		return userDevice;
	}

	@Transactional(rollbackOn = {Exception.class, ApiControllerException.class})
	public UserApi<UserResponse> loginUserETC(User user)  throws ApiControllerException, Exception { 
		
		
		UserApi<UserResponse> api = new UserApi<UserResponse>();
		
		ApiUserDevice.OsType osTypeEnum = (ApiUserDevice.OsType) DBUtility.convertToEnumByString(Arrays.asList(ApiUserDevice.OsType.values()), user.getOsTypeParam());
		
		if(osTypeEnum == null) {
			throw new ApiControllerException(UserApi.RES_STATUS_CODE_FOR_ERROR_IS_INPUT_PARAMS_INVALID);
		}
		
		
		ApiUser loginedUser = userRepo.findByUsername(user.getUserId());
		
		if(loginedUser == null) {
			throw new ApiControllerException(UserApi.RES_STATUS_CODE_FOR_FAILED_IS_LOGINED);
		}
		
		boolean matchThePassword = encoder.matches(user.getPassword(), loginedUser.getPassword());
		
		if(!matchThePassword) {
			throw new ApiControllerException(UserApi.RES_STATUS_CODE_FOR_FAILED_IS_LOGINED);
		}
		
		if(loginedUser.getStatus() == ApiUser.UserStatus.FORCED_CHECKOUT) {
			throw new ApiControllerException(UserApi.RES_STATUS_CODE_FOR_FAILED_IS_USER_NOT_USE);
		}
		loginedUser.setStatus(ApiUser.UserStatus.USE);
		loginedUser.setLatestLogin(new Date());
		
		loginedUser = userRepo.save(loginedUser);
		if(loginedUser == null) {
			throw new ApiControllerException(UserApi.RES_STATUS_CODE_FOR_ERROR_IS_DB_FAILED);
		}
		
		ApiUserDevice userDevice = registerDevice(loginedUser, user);
		
		UserResponse response = new UserResponse();
		
		response.setUser(loginedUser);
		response.setExpiredDate(userDevice.getLoginExpired());
		response.setAuthToken(userDevice.getAuthToken());
		api.addModel(response);

		return api;
	}
	
	
}
