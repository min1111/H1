/*
 * engine
 * Copyright 2019. 4. 12. Hyper Link all rights reserved.
 */

 

package kr.hyperlink.server.engine.api.v1.controller;

import kr.hyperlink.server.engine.utility.FileHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * <pre>
 * kr.hyperlink.server.engine.api.controller 
 * ImageController.java
 * @author 			:	Hyper Link 심현섭
 * @date 			:	2019. 4. 12.
 * @brief			:   이미지 Controller
 * @description		:	
 * </pre>
 *
 */
@RestController
@CrossOrigin(origins = "*", 
				allowedHeaders = "X-Auth-Token", 
				methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT, RequestMethod.PATCH, RequestMethod.DELETE})
@RequestMapping(value = "/api/images")
public class ImageController {
	
	@Autowired
	private FileHandler fileHandler;
	
	@ResponseBody 
	@RequestMapping(value="/{type}/{fileName:.+}", method = RequestMethod.GET, produces = {MediaType.IMAGE_JPEG_VALUE, MediaType.IMAGE_PNG_VALUE})
	public ResponseEntity<byte[]> getImageAsResource(
			@Validated @PathVariable("type") String type,
			@Validated @PathVariable("fileName") String fileName) {
		ResponseEntity<byte[]> responseEntity = null;
		byte[] file = null;
		try {
			String path = new StringBuffer().append(type).append("/").append(fileName).toString();
			boolean existsFile = fileHandler.existsFile(path);
			if(existsFile) {
				file = fileHandler.getFile(path);
			} else {
				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			responseEntity = new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		responseEntity = new ResponseEntity<>(file, HttpStatus.OK);
		return responseEntity;
	}

}
