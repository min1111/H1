/*
 * engine
 * Copyright 2019. 4. 11. Hyper Link all rights reserved.
 */

 

package kr.hyperlink.server.engine.api.model;

import java.io.Serializable;
import java.util.Date;

import kr.hyperlink.server.engine.api.domain.ApiUser;

/**
 * <pre>
 * kr.hyperlink.server.engine.api.model 
 * UserResponse.java
 * @author 			:	Hyper Link 심현섭
 * @date 			:	2019. 4. 11.
 * @brief			:   유저 로그인 Response Model
 * @description		:	
 * </pre>
 *
 */

public class UserResponse  implements Serializable {

	/** <code>serialVersionUID</code> : */
	private static final long serialVersionUID = -8357932138358237555L;
	
	/** <code>authToken</code> : 인증토큰 */
	private String authToken;
	
	/** <code>expiredDate</code> : 인증토큰 만료시간 */
	private Date expiredDate;
	
	/** <code>user</code> : 로그인 유저 객체 */
	private ApiUser user;

	/**
	 * @brief authToken를 전달한다.
	 *
	 * @return authToken
	 */
	public String getAuthToken() {
		return authToken;
	}

	/**
	 * @brief authToken을 설정한다. 
	 *
	 * @param authToken authToken 
	 */
	
	public void setAuthToken(String authToken) {
		this.authToken = authToken;
	}

	/**
	 * @brief expiredDate를 전달한다.
	 *
	 * @return expiredDate
	 */
	public Date getExpiredDate() {
		return expiredDate;
	}

	/**
	 * @brief expiredDate을 설정한다. 
	 *
	 * @param expiredDate expiredDate 
	 */
	
	public void setExpiredDate(Date expiredDate) {
		this.expiredDate = expiredDate;
	}

	/**
	 * @brief user를 전달한다.
	 *
	 * @return user
	 */
	public ApiUser getUser() {
		return user;
	}

	/**
	 * @brief user을 설정한다. 
	 *
	 * @param user user 
	 */
	
	public void setUser(ApiUser user) {
		this.user = user;
	}
	
	
	

}
