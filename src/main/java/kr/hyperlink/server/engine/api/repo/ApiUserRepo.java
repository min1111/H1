/*
 * engine
 * Copyright 2019. 4. 11. Hyper Link all rights reserved.
 */

 

package kr.hyperlink.server.engine.api.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import kr.hyperlink.server.engine.api.domain.ApiUser;

/**
 * <pre>
 * kr.hyperlink.server.engine.api.repo 
 * ApiUserRepo.java
 * @author 			:	Hyper Link 심현섭
 * @date 			:	2019. 4. 11.
 * @brief			:   일반 유저 Repo
 * @description		:	
 * </pre>
 *
 */

@Repository
public interface ApiUserRepo  extends JpaRepository<ApiUser, Long> {

	public int countByUsername(String userId);

	public int countByEmail(String email);

	public ApiUser findByUserIDSNS(String userId);

	public ApiUser findByUsername(String userId); 

}
