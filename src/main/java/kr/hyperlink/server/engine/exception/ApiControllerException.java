/*
 * engine
 * Copyright 2019. 4. 11. Hyper Link all rights reserved.
 */

 

package kr.hyperlink.server.engine.exception;

import kr.hyperlink.server.engine.api.model.UserApi;

/**
 * <pre>
 * kr.hyperlink.server.engine.exception 
 * ApiControllerException.java
 * @author 			:	Hyper Link 심현섭
 * @date 			:	2019. 4. 11.
 * @brief			:   REST ful API 예외 클래스
 * @description		:	
 * </pre>
 *
 */

public class ApiControllerException  extends Exception {

	/** <code>serialVersionUID</code> : */
	private static final long serialVersionUID = 90489826627564629L;
	
	private String code = null;
	
	public ApiControllerException () {
		this.code = UserApi.RES_STATUS_CODE_FOR_ERROR_IS_SERVER_ERROR;
	}
	
	/**
	 * @brief 생성자  Api Controller Exception Handler
	 *
	 * @param message
	 */
	public ApiControllerException(String code) {
		this.code = code;
	}
	
	
	/**
	 * @brief 생성자  Api Controller Exception Handler
	 *
	 * @param cause 에러 예외 객체
	 */
	public ApiControllerException(Throwable cause) {
		super(cause);
	}
	
	/**
	 * @brief 생성자  Api Controller Exception Handler
	 *
	 * @param code 에러 코드
	 * @param message 에러 메세지
	 */
	public ApiControllerException(String code, String message) {
		super(message);
		this.code = code;
	}
	

	/**
	 * @brief 생성자  Api Controller Exception Handler 
	 *
	 * @param code 에러 코드
	 * @param message 에러 메세지
	 * @param cause 에러 예외 객체
	 */
	public ApiControllerException(String code, String message, Throwable cause) {
		super(message, cause);
		this.code = code;
	}
	
	/**
	 * @brief  에러 코드를 반환한다. 
	 *
	 * @return 에러 코드
	 */
	public String getCode () {
		if(code != null) {
			return code;
		}
		return "R500";
	}
	
}
