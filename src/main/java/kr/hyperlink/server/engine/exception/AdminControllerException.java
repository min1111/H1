/*
 * engine
 * Copyright 2019. 4. 11. Hyper Link all rights reserved.
 */

 

package kr.hyperlink.server.engine.exception;

/**
 * <pre>
 * kr.hyperlink.server.engine.exception 
 * AdminControllerException.java
 * @author 			:	Hyper Link 심현섭
 * @date 			:	2019. 4. 11.
 * @brief			:   관리자 처리 예외 클래스
 * @description		:	
 * </pre>
 *
 */

public class AdminControllerException extends Exception {

	/** <code>serialVersionUID</code> : */
	private static final long serialVersionUID = -2800353626809126942L;
	
	private String code = null;
	
	/**
	 * @brief 생성자 Admin Controller Exception Handler
	 *
	 */
	public AdminControllerException() {
		
	}
	
	/**
	 * @brief 생성자 Admin Controller Exception Handler
	 *
	 * @param message
	 */
	public AdminControllerException(String code) {
		this.code = code;
	}
	
	/**
	 * @brief 생성자 Admin Controller Exception Handler
	 *
	 * @param cause 에러 예외 객체
	 */
	public AdminControllerException(Throwable cause) {
		super(cause);
	}
 
	/**
	 * @brief 생성자  Admin Controller Exception Handler
	 *
	 * @param message 에러 메세지
	 * @param cause 에러 예외 객체
	 */
	public AdminControllerException(String message, Throwable cause) {
		super(message, cause);
	}
	
	/**
	 * @brief 생성자 Admin Controller Exception Handler
	 *
	 * @param code 에러 코드
	 * @param message 에러 메세지
	 */
	public AdminControllerException(String code, String message) {
		super(message);
		this.code = code;
	}
	
	/**
	 * @brief 생성자  Admin Controller Exception Handler 
	 *
	 * @param code 에러 코드
	 * @param message 에러 메세지
	 * @param cause 에러 예외 객체
	 */
	public AdminControllerException(String code, String message, Throwable cause) {
		super(message, cause);
		this.code = code;
	}
	
	/**
	 * @brief  에러 코드를 반환한다. 
	 *
	 * @return 에러 코드
	 */
	public String getCode () {
		if(code != null) {
			return code;
		}
		return "R500";
	}

}
