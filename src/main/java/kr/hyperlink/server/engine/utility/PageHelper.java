/*
 * server
 * Copyright 2020. 6. 8. Hyper Link all rights reserved.
 */

 

package kr.hyperlink.server.engine.utility;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;

/**
 * <pre>
 * kr.hyperlink.server.engine.utility 
 * PageHelper.java
 * @author 			:	Hyper Link 심현섭
 * @date 			:	2020. 6. 8.
 * @brief			:   
 * @description		:	
 * </pre>
 *
 */

public class PageHelper {

	
	/**
	 * Returns a new object which specifies the the wanted result page.
	 * 
	 * @param pageIndex
	 *            fisrt page 1, ...
	 * @return
	 */
	public static Pageable constructPageSpecification(int pageIndex, int max) {
		Pageable pageSpecification = PageRequest.of(--pageIndex, max, Sort.Direction.DESC, "id");
				//CommonModel_.createDate.getName());
		return pageSpecification;
	}
	
	public static Pageable constructPageSpecificationWithoutOrder(int pageIndex, int max) {
		Pageable pageSpecification = PageRequest.of(--pageIndex, max);
				//CommonModel_.createDate.getName());
		return pageSpecification;
	}
	

	public static Pageable constructPageSpecification(int pageIndex, int max, Direction sortOrder, String[] sortColumns) {
		Pageable pageSpecification = PageRequest.of(--pageIndex, max, sortOrder, sortColumns);
		return pageSpecification;
	}

	public static Pageable constructPageSpecification(int pageIndex, int max, Direction sortOrder, String sortColumn) {
		Pageable pageSpecification = PageRequest.of(--pageIndex, max, sortOrder, sortColumn);
		return pageSpecification;
	}
}
