/*
 * engine
 * Copyright 2019. 4. 10. Hyper Link all rights reserved.
 */

 

package kr.hyperlink.server.engine.constants;

import java.io.Serializable;

/**
 * <pre>
 * kr.hyperlink.server.engine.constants 
 * DomainTableConstants.java
 * @author 			:	Hyper Link 심현섭
 * @date 			:	2019. 4. 10.
 * @brief			:   테이블명 상수 모음
 * @description		:	
 * </pre>
 *
 */

public class DomainTableConstants implements Serializable {

	/** <code>serialVersionUID</code> : */
	private static final long serialVersionUID = -2132873496147137756L;
	
	/** <code>TBL_BASE</code> : DB 구분자 */
	private static final String TBL_BASE = "TEST_"; 
	
	/** <code>TBL_ADMIN_USER</code> : 관리자 테이블  */
	public static final String TBL_ADMIN_USER = TBL_BASE + "ADMIN_USER";
	
	/** <code>TBL_ADMIN_PROPERTIES</code> : 관리자 페이지 설정 테이블 */
	public static final String TBL_ADMIN_PROPERTIES = TBL_BASE + "ADMIN_PROPERTIES";
	
	/** <code>TBL_API_USER</code> : 유저 테이블 */
	public static final String TBL_API_USER = TBL_BASE + "API_USER";
	
	/** <code>TBL_API_USER_DEVICE</code> : 유저 장치 테이블 */
	public static final String TBL_API_USER_DEVICE = TBL_BASE + "API_USER_DEVICE";
	
	

}
