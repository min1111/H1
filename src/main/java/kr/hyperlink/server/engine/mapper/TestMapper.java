package kr.hyperlink.server.engine.mapper;

import kr.hyperlink.server.engine.config.DataMapper;

import java.util.List;

@DataMapper
public interface TestMapper {

    public List<String> findTest();
}
