/*
 * engine
 * Copyright 2019. 4. 11. Hyper Link all rights reserved.
 */

 

package kr.hyperlink.server.engine.aspect;

import javax.servlet.http.HttpServletRequest;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import kr.hyperlink.server.engine.utility.StringUtility;
import lombok.extern.slf4j.Slf4j;

/**
 * <pre>
 * kr.hyperlink.server.engine.aspect 
 * EngineAspect.java
 * @author 			:	Hyper Link 심현섭
 * @date 			:	2019. 4. 11.
 * @brief			:   Server Aspect 처리 클래스
 * @description		:	
 * </pre>
 *
 */
@Slf4j
@Aspect
@Order(5)
@Component
public class EngineAspect {
	
	ThreadLocal<Long> startTime = new ThreadLocal<>();
	
	@Pointcut("execution(public * kr.hyperlink.server.engine.api.controller..*.*(..))")
    public void userLog(){}
	
	@Pointcut("execution(public * kr.hyperlink.server.engine.admin.controller..*.*(..))")
    public void adminLog(){}
	
	@Before("userLog()")
    public void doBefore(JoinPoint joinPoint) throws Throwable {
        startTime.set(System.currentTimeMillis());
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = attributes.getRequest();
        if(!(request.getRequestURL().toString().contains("/api/images/") || request.getRequestURL().toString().endsWith("/updateFile"))) {
        	log.debug("[URL : {}, HTTP_METHOD : {} , IP : {}]]", request.getRequestURL().toString(), request.getMethod(), request.getRemoteAddr());
            log.debug("[CLASS_METHOD : {}]", joinPoint.getSignature().getDeclaringTypeName() + "." + joinPoint.getSignature().getName());
            log.debug("[ARGS : {}]", StringUtility.prettyPrintLog(joinPoint.getArgs()));
        }
    }
	
	@AfterReturning(returning = "ret", pointcut = "userLog()")
    public void doAfterReturning(Object ret) throws Throwable {
		log.debug("[SPEND TIME : {}]", (System.currentTimeMillis() - startTime.get()));
    }

	
	@Before("adminLog()")
    public void doAdminBefore(JoinPoint joinPoint) throws Throwable {
        startTime.set(System.currentTimeMillis());
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = attributes.getRequest();
        if(!(request.getRequestURL().toString().contains("/api/images/") || request.getRequestURL().toString().contains("/updateFile"))) {
        	log.debug("[URL : {}, HTTP_METHOD : {} , IP : {}]]", request.getRequestURL().toString(), request.getMethod(), request.getRemoteAddr());
            log.debug("[CLASS_METHOD : {}]", joinPoint.getSignature().getDeclaringTypeName() + "." + joinPoint.getSignature().getName());
            log.debug("[ARGS : {}]", StringUtility.prettyPrintLog(joinPoint.getArgs()));
        }
    }
	
	@AfterReturning(returning = "ret", pointcut = "adminLog()")
    public void doAdminAfterReturning(Object ret) throws Throwable {
		log.debug("[SPEND TIME : {}]", (System.currentTimeMillis() - startTime.get()));
    }

}
